package com.doddysujatmiko.gold.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Entity(name = "orders")
@Getter
@Setter
public class Order extends AbstractEntity implements Serializable {
    @Column(name = "destination_address")
    private String destinationAddress;

    @Column(name = "completed")
    private Boolean completed;

//    @JsonIgnore
//    public User getUser() {
//        return user;
//    }
//
//    @JsonProperty
//    public void setUser(User user) {
//        this.user = user;
//    }

//    @JsonIgnore
    @ManyToOne(targetEntity = User.class)
    @JoinColumn(name="user_id", referencedColumnName = "id")
    private User user;

//    @JsonIgnore
    @OneToMany(mappedBy = "order", fetch = FetchType.EAGER)
    private List<OrderDetail> orderDetails;

//    @JsonIgnore
//    public List<OrderDetail> getOrderDetails() {
//        return orderDetails;
//    }
//
//    @JsonProperty
//    public void setOrderDetails(List<OrderDetail> orderDetails) {
//        if(orderDetails.equals(this.orderDetails)) {
//            return;
//        }
//        this.orderDetails = orderDetails;
//    }
}
