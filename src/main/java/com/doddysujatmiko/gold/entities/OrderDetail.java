package com.doddysujatmiko.gold.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Entity(name = "order_details")
@Getter
@Setter
public class OrderDetail extends AbstractEntity implements Serializable {
    @Column(name = "quantity")
    private Integer quantity;

    @JsonIgnore
    @Column(name = "total_price")
    private Integer totalPrice;

    @JsonProperty
    public Integer getTotalPrice() {
        return totalPrice;
    }

    @JsonIgnore
    public void setTotalPrice(Integer totalPrice) {
        this.totalPrice = totalPrice;
    }

//    @JsonIgnore
    @ManyToOne(targetEntity = Product.class)
    @JoinColumn(name="product_id", referencedColumnName = "id")
    private Product product;

    @JsonIgnore
    @ManyToOne(targetEntity = Order.class)
    @JoinColumn(name="order_id", referencedColumnName = "id")
    private Order order;
}
