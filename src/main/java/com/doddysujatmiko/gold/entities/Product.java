package com.doddysujatmiko.gold.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Entity(name = "products")
@Getter
@Setter
public class Product extends AbstractEntity implements Serializable {
    @Column(name = "name")
    private String name;

    @Column(name = "price")
    private Integer price;

    @JsonIgnore
    @ManyToOne(targetEntity = Merchant.class)
    @JoinColumn(name="merchant_id", referencedColumnName = "id")
    private Merchant merchant;

    @JsonIgnore
    public Merchant getMerchant() {
        return merchant;
    }

    @JsonProperty
    public void setMerchant(Merchant merchant){
        if(merchant.equals(this.merchant)) {
            return;
        }
        this.merchant = merchant;
    }

    @JsonIgnore
    @OneToMany(mappedBy = "product", fetch = FetchType.EAGER)
    private List<OrderDetail> orderDetails;
}
