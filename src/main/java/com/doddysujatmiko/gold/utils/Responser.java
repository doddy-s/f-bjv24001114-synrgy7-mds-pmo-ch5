package com.doddysujatmiko.gold.utils;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class Responser {

    public static ResponseEntity<Map<Object, Object>> constructSuccess(Object obj, String message, HttpStatus status){
        Map<Object, Object> map = new HashMap<>();
        map.put("data", obj);
        map.put("message", message);
        return new ResponseEntity<>(map, status);
    }

    public static ResponseEntity<Map<Object, Object>> constructError(String message, HttpStatus status){
        Map<Object, Object> map = new HashMap<>();
        map.put("message", message);
        return new ResponseEntity<>(map, status);
    }
}