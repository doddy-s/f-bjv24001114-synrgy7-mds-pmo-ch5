package com.doddysujatmiko.gold.utils;

import com.doddysujatmiko.gold.exceptions.BadRequestException;
import com.doddysujatmiko.gold.exceptions.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Map;

public class ErrorHandler {
    public static ResponseEntity<Map<Object, Object>> handleError(Throwable t) {
        if(t instanceof NotFoundException) {
            return Responser.constructError(t.getMessage(), HttpStatus.NOT_FOUND);
        }

        if(t instanceof BadRequestException) {
            return Responser.constructError(t.getMessage(), HttpStatus.BAD_REQUEST);
        }

        return Responser.constructError(t.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
