package com.doddysujatmiko.gold.controllers;

import com.doddysujatmiko.gold.entities.User;
import com.doddysujatmiko.gold.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping("v1/auth")
public class AuthController {
    @Autowired
    private UserService userService;

    @PostMapping("register")
    public ResponseEntity<Map<Object, Object>> register(@RequestBody User user) {
        return userService.register(user);
    }

    @GetMapping("{userId}")
    public ResponseEntity<Map<Object, Object>> getUserById(@PathVariable UUID userId) {
        return userService.read(userId);
    }

    @PutMapping("")
    public ResponseEntity<Map<Object, Object>> putUser(@RequestBody User user) {
        return userService.update(user);
    }

    @DeleteMapping("{userId}")
    public ResponseEntity<Map<Object, Object>> deleteUserById(@PathVariable UUID userId) {
        return userService.delete(userId);
    }

    @PostMapping("login")
    public ResponseEntity<Map<Object, Object>> login(@RequestBody User user) {
        return userService.login(user);
    }

    @GetMapping("page")
    public ResponseEntity<Map<Object, Object>> list(@RequestParam(required = false, name = "name") String name,
                                                    @RequestParam(required = false, name = "page") Integer page,
                                                    @RequestParam(required = false, name = "size") Integer size) {
        return userService.readFilteredPage(page, size, name);
    }
}
