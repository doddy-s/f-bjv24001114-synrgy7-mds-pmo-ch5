package com.doddysujatmiko.gold.controllers;

import com.doddysujatmiko.gold.entities.Merchant;
import com.doddysujatmiko.gold.services.MerchantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping("v1/merchant")
public class MerchantController {
    @Autowired
    private MerchantService merchantService;

    @PostMapping("")
    public ResponseEntity<Map<Object, Object>> postMerchant(@RequestBody Merchant merchant) {
        return merchantService.create(merchant);
    }

    @GetMapping("{merchantId}")
    public ResponseEntity<Map<Object, Object>> getMerchantById(@PathVariable UUID merchantId) {
        return merchantService.read(merchantId);
    }

    @PutMapping("")
    public ResponseEntity<Map<Object, Object>> putMerchant(@RequestBody Merchant merchant) {
        return merchantService.update(merchant);
    }

    @DeleteMapping("{merchantId}")
    public ResponseEntity<Map<Object, Object>> deleteMerchant(@PathVariable UUID merchantId) {
        return merchantService.delete(merchantId);
    }

    @GetMapping("page")
    public ResponseEntity<Map<Object, Object>> list(@RequestParam(required = false, name = "name") String name,
                                    @RequestParam(required = false, name = "page") Integer page,
                                    @RequestParam(required = false, name = "size") Integer size) {
        return merchantService.readFilteredPage(page, size, name);
    }
}
