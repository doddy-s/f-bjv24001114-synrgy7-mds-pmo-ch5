package com.doddysujatmiko.gold.controllers;

import com.doddysujatmiko.gold.entities.Order;
import com.doddysujatmiko.gold.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping("v1/order")
public class OrderController {
    @Autowired
    private OrderService orderService;

    @PostMapping("")
    public ResponseEntity<Map<Object, Object>> postOrder(@RequestBody Order order) {
        return orderService.create(order);
    }

    @GetMapping("{orderId}")
    public ResponseEntity<Map<Object, Object>> getOrderById(@PathVariable UUID orderId) {
        return orderService.read(orderId);
    }

    @PutMapping("")
    public ResponseEntity<Map<Object, Object>> putOrder(@RequestBody Order order) {
        return orderService.update(order);
    }

    @PatchMapping("additem")
    public ResponseEntity<Map<Object, Object>> addItem(@RequestParam(required = true, name = "orderId") UUID orderId,
                                                       @RequestParam(required = true, name = "orderDetailId") UUID orderDetailId) {
        return orderService.addItem(orderId, orderDetailId);
    }

    @DeleteMapping("{orderId}")
    public ResponseEntity<Map<Object, Object>> deleteOrder(@PathVariable UUID orderId) {
        return orderService.delete(orderId);
    }

    @GetMapping("page")
    public ResponseEntity<Map<Object, Object>> list(@RequestParam(required = false, name = "name") String name,
                                                    @RequestParam(required = false, name = "userId") UUID userId,
                                                    @RequestParam(required = false, name = "page") Integer page,
                                                    @RequestParam(required = false, name = "size") Integer size) {
        return orderService.readFilteredPage(page, size, name, userId);
    }
}
