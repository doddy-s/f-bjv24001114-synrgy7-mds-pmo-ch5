package com.doddysujatmiko.gold.controllers;

import com.doddysujatmiko.gold.entities.Product;
import com.doddysujatmiko.gold.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping("v1/product")
public class ProductController {
    @Autowired
    private ProductService productService;

    @PostMapping("")
    public ResponseEntity<Map<Object, Object>> postProduct(@RequestBody Product product) {
        return productService.create(product);
    }

    @GetMapping("{productId}")
    public ResponseEntity<Map<Object, Object>> getProductById(@PathVariable UUID productId) {
        return productService.read(productId);
    }

    @PutMapping("")
    public ResponseEntity<Map<Object, Object>> putProduct(@RequestBody Product product) {
        return productService.update(product);
    }

    @DeleteMapping("{productId}")
    public ResponseEntity<Map<Object, Object>> deleteProduct(@PathVariable UUID productId) {
        return productService.delete(productId);
    }

    @GetMapping("page")
    public ResponseEntity<Map<Object, Object>> list(@RequestParam(required = false, name = "name") String name,
                                                    @RequestParam(required = false, name = "merchantId") UUID merchantId,
                                                    @RequestParam(required = false, name = "page") Integer page,
                                                    @RequestParam(required = false, name = "size") Integer size) {
        return productService.readFilteredPage(page, size, name, merchantId);
    }
}
