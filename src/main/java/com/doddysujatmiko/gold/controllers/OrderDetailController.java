package com.doddysujatmiko.gold.controllers;

import com.doddysujatmiko.gold.entities.OrderDetail;
import com.doddysujatmiko.gold.services.OrderDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping("v1/orderDetail")
public class OrderDetailController {
    @Autowired
    private OrderDetailService orderDetailService;

    @PostMapping("")
    public ResponseEntity<Map<Object, Object>> postOrderDetail(@RequestBody OrderDetail orderDetail) {
        return orderDetailService.create(orderDetail);
    }

    @GetMapping("{orderDetailId}")
    public ResponseEntity<Map<Object, Object>> getOrderDetailById(@PathVariable UUID orderDetailId) {
        return orderDetailService.read(orderDetailId);
    }

    @PutMapping("")
    public ResponseEntity<Map<Object, Object>> putOrderDetail(@RequestBody OrderDetail orderDetail) {
        return orderDetailService.update(orderDetail);
    }

    @DeleteMapping("{orderDetailId}")
    public ResponseEntity<Map<Object, Object>> deleteOrderDetail(@PathVariable UUID orderDetailId) {
        return orderDetailService.delete(orderDetailId);
    }

    @GetMapping("page")
    public ResponseEntity<Map<Object, Object>> list(@RequestParam(required = false, name = "merchantId") UUID merchantId,
                                                    @RequestParam(required = false, name = "page") Integer page,
                                                    @RequestParam(required = false, name = "size") Integer size) {
        return orderDetailService.readFilteredPage(page, size, merchantId);
    }
}
