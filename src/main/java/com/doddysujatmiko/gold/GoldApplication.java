package com.doddysujatmiko.gold;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@OpenAPIDefinition
public class GoldApplication {

    public static void main(String[] args) {
        SpringApplication.run(GoldApplication.class, args);
    }

}
