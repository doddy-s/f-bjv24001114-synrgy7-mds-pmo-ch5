package com.doddysujatmiko.gold.exceptions;

public class BadRequestException extends RuntimeException{
    public BadRequestException(String s) {
        super(s);
    }
}
