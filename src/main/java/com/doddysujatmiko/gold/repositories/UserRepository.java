package com.doddysujatmiko.gold.repositories;

import com.doddysujatmiko.gold.entities.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface UserRepository extends JpaRepository<User, UUID>, JpaSpecificationExecutor<User> {

    public Optional<User> findByEmail(String email);

    // 1. DTO

    // 2. Query object-nya

    // 3. Native Query

    // 4. Pagination

    // 5. Procedure
    @Procedure
    User create_user(String username, String email, String password);
}
