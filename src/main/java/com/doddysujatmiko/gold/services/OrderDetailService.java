package com.doddysujatmiko.gold.services;

import com.doddysujatmiko.gold.entities.OrderDetail;
import org.springframework.http.ResponseEntity;

import java.util.Map;
import java.util.UUID;

public interface OrderDetailService {
    ResponseEntity<Map<Object, Object>> create(OrderDetail orderDetail);

    ResponseEntity<Map<Object, Object>> read(UUID id);

    ResponseEntity<Map<Object, Object>> update(OrderDetail orderDetail);

    ResponseEntity<Map<Object, Object>> delete(UUID id);

    ResponseEntity<Map<Object, Object>> readFilteredPage(Integer page, Integer size, UUID productId);
}
