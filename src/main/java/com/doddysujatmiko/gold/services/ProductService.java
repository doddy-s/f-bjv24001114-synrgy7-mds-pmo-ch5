package com.doddysujatmiko.gold.services;

import com.doddysujatmiko.gold.entities.Product;
import org.springframework.http.ResponseEntity;

import java.util.Map;
import java.util.UUID;

public interface ProductService {
    ResponseEntity<Map<Object, Object>> create(Product product);

    ResponseEntity<Map<Object, Object>> read(UUID id);

    ResponseEntity<Map<Object, Object>> update(Product product);

    ResponseEntity<Map<Object, Object>> delete(UUID id);

    ResponseEntity<Map<Object, Object>> readFilteredPage(Integer page, Integer size, String productName, UUID merchantId);
}
