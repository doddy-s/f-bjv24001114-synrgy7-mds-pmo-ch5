package com.doddysujatmiko.gold.services;

import com.doddysujatmiko.gold.entities.Merchant;
import org.springframework.http.ResponseEntity;

import java.util.Map;
import java.util.UUID;

public interface MerchantService {
    ResponseEntity<Map<Object, Object>> create(Merchant merchant);

    ResponseEntity<Map<Object, Object>> read(UUID id);

    ResponseEntity<Map<Object, Object>> update(Merchant merchant);

    ResponseEntity<Map<Object, Object>> delete(UUID id);

    ResponseEntity<Map<Object, Object>> readFilteredPage(Integer page, Integer size, String merchantName);
}
